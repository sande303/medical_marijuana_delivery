require 'spec_helper'

describe User do

  before do
    @user = User.new(first_name: "first name",last_name: "last name", email: "user@example.com", password: "password", password_confirmation: "password", address: "address", city: "city", state: "state", zip: "zip")
  end

  subject { @user }

  it { should respond_to(:first_name) }
  it { should respond_to(:last_name) }
  it { should respond_to(:email) }
  it { should respond_to(:password) }
  it { should respond_to(:password_confirmation) }
  it { should respond_to(:address) }
  it { should respond_to(:city) }
  it { should respond_to(:state) }
  it { should respond_to(:zip) }

  it { should be_valid }


  describe "when first name is not present" do
    before { @user.first_name = " " }
    it { should_not be_valid }
  end

  describe "when last name is not present" do
    before { @user.last_name = " " }
    it { should_not be_valid }
  end

  describe "when first name is too long" do
    before { @user.first_name = "a"*31 }
    it { should_not be_valid }
  end

  describe "when last name is too long" do
    before { @user.last_name = "a"*31 }
    it { should_not be_valid }
  end

  describe "when address is not present" do
    before { @user.address = " " }
    it { should_not be_valid }
  end

  describe "when city is not present" do
    before { @user.city = " " }
    it { should_not be_valid }
  end

  describe "when state is not present" do
    before { @user.state = " " }
    it { should_not be_valid }
  end

  describe "when zip is not present" do
    before { @user.zip = " " }
    it { should_not be_valid }
  end
end
