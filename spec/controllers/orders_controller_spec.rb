require 'spec_helper'


 describe Api::V1::OrdersController do


   describe "GET index" do

     it "should require an authentication token" do
       get :index, :authentication_token => ''
       json = JSON.parse(response.body)
       json.should include("success" => false, "status" => 401, "message" => 'Error with your credentials')
     end
 #
 #     it "should respond with json" do
 #       get :index, :authentication_token => @user.authentication_token, :format => :json
 #       json = JSON.parse(response.body)
 #       json.should include("success" => true, "status" => 200, "orders" => [JSON.parse(@order.to_builder.attributes!.to_json) ] )
 #     end
   end
 #
 #   describe "GET show" do
 #
 #     it "should require an authentication token" do
 #       get :show, :id => @order.id, :authentication_token => ''
 #       json = JSON.parse(response.body)
 #       json.should include("success" => false, "status" => 401, "message" => 'Error with your credentials')
 #     end
 #
 #     it "should respond with order json if not found" do
 #       get :show, :id => 0, :authentication_token => @user.authentication_token
 #       json = JSON.parse(response.body)
 #       json.should include("success" => false, "status" => 400, "message" => 'Order does not exist')
 #     end
 #
 #     it "should respond with json if found" do
 #       get :show, :id => @order.id, :authentication_token => @user.authentication_token
 #       json = JSON.parse(respnse.body)
 #       json.should include("success" => true, "status" => 200, "order" => JSON.parse(@order.to_builder.attributes!.to_json))
 #     end
 #   end
 #
 end
