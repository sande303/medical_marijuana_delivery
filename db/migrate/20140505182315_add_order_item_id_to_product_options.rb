class AddOrderItemIdToProductOptions < ActiveRecord::Migration
  def change
    add_column :product_options, :order_item_id, :integer
  end
end
