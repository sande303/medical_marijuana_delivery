class AddProductImageToPicture < ActiveRecord::Migration
  def change
    add_attachment :pictures, :product_image
  end
end
