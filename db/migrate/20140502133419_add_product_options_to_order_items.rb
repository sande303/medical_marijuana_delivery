class AddProductOptionsToOrderItems < ActiveRecord::Migration
  def change
      add_column :order_items, :product_option_id, :integer
  end
end
