ActiveAdmin.register User do

  index do
    column :id
    column :email
    column :first_name
    column :last_name
    column :created_at
    column :updated_at
    column :address
    column :city
    column :state
    column :zip
    column :password
    column :password_confirmation
    column :registered
    column :authentication_token
    default_actions
  end

  form do |f|
    f.inputs do
      f.input :email
      f.input :first_name
      f.input :last_name
      f.input :address
      f.input :city
      f.input :state
      f.input :zip
      f.input :registered
      if f.object.new_record?
        f.input :password
        f.input :password_confirmation
      end
    end
    f.actions
  end


  controller do
    def permitted_params
      params.permit :utf8, :authentication_token, :commit, :id, user: [:email, :first_name, :last_name, :address, :city, :state, :zip, :password, :password_confirmation, :authentication_token]
    end
  end
end
