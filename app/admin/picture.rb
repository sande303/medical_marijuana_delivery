ActiveAdmin.register Picture do

  index do
    column :id
    column :product
    column :created_at
    column :updated_at
    column :product_image_file_name
    column :product_image_file_size do |size|
      number_to_human_size(size.product_image_file_size)
    end
    column :product_image_updated_at
    default_actions
  end

  form do |f|
    f.inputs "Product Pictures" do
      f.input :product
      f.input :product_image, :as => :file, :hint => f.template.image_tag(f.object.product_image.url(:thumb))
    end
    f.actions
  end

  show do |product|
    attributes_table do
      row :product
      row :product_image do
        image_tag(product.product_image.url(:thumb))
      end
    end
  end

  controller do
    def permitted_params
      params.permit :id, :authenticity_token, :commit, :utf8, picture: [:url, :product_image, :content_type, :product_id]
    end
  end

end
