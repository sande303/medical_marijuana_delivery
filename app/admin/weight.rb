ActiveAdmin.register Weight do


  index do
    column :id
    column :name
    default_actions
  end

  form do |f|
    f.inputs do
      f.input :name
    end
    f.actions
  end

  controller do
    def permitted_params
      params.permit :utf8, :authenticity_token, :commit, :id, weight: [:name]
    end
  end
end
