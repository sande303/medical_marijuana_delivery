ActiveAdmin.register ProductOption do

  index do
    column :id
    column :product
    column :weight
    column :price do |p|
      number_to_currency(p.price)
    end
    default_actions
  end

  form do |f|
    f.inputs do
      f.input :product
      f.input :weight
      f.input :price
    end
    f.actions
  end

  controller do
    def permitted_params
      params.permit :utf8, :authenticity_token, :commit, :id, product_option: [:product_id, :price, :weight_id]
    end
  end

end
