ActiveAdmin.register Order do

  filter :user
  filter :created_at

  show do
    attributes_table do
      row :transaction_id do |order|
        "#{order.transaction_id}"
      end
    end
  end

  index do
    column :id
    column :order
    column :user
    column :shipping_address
    column :status
    column :transaction_total
    column :transaction_id
    default_actions
  end

  form do |f|
    f.inputs do

      f.input :user
      f.input :product_option_id
      f.input :shipping_address
      f.input :status
    end
    f.actions
  end

  controller do
    def permitted_params
      params.permit :utf8, :id, :authenticity_token, :commit, order: [:product_id, :user, :order_id, :product_option_id, :shipping_address, :status]
    end
  end
end
