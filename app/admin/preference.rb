ActiveAdmin.register Preference do


  index do
    column :id
    column :shipping_fee do |p|
      number_to_currency(p.shipping_fee)
    end
    default_actions
  end

  form do |f|
    f.inputs do
      f.input :shipping_fee
    end
    f.actions
  end

  controller do
    def permitted_params
      params.permit :utf8, :authenticity_token, :commit, :id, preference: [:shipping_fee]
    end
  end

end
