ActiveAdmin.register Product do


  index do
    column :id
    column :name
    column :description
    default_actions
  end

  form do |f|
    f.inputs do
      f.input :name
      f.input :description
    end
    f.actions
  end

  controller do
    def permitted_params
      params.permit :utf8, :authenticity_token, :commit, :id, product: [:name, :description]
    end


  end
end
