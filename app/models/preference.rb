class Preference < ActiveRecord::Base

  def to_builder
    Jbuilder.new do |json|
      json.id id
      json.shipping_fee shipping_fee
    end
  end

end
