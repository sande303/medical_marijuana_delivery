class Picture < ActiveRecord::Base
  belongs_to :product
  has_attached_file :product_image, 
  									styles: {
  										thumb: "195",
  										large: "700"
  									},
  									storage: :s3,
  									bucket: ENV['AWS_BUCKET'],
  									s3_credentials: {
  										access_key_id: ENV['AWS_ACCESS_KEY'],
  										secret_access_key: ENV['AWS_SECRET_KEY']
  									},
  									path: "picture-:id/images/:style/:hash.:extension",
  									hash_secret: "secret"

  validates_attachment_content_type :product_image, :content_type => /\Aimage\/.*\Z/

  def s3_url
  	self.product_image.url rescue ""
  end

  def to_builder
  	Jbuilder.new do |json|
  		json.product_image s3_url
  	end
  end


end
