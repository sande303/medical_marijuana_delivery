class Order < ActiveRecord::Base
  belongs_to :user
  has_many :order_items
#  has_many :product_options
  has_many :preferences

  def to_builder
    Jbuilder.new do |json|
      json.id id
      json.transaction_total transaction_total
      json.order_items order_items.map { |order_item| order_item.to_builder.attributes!}
      json.date created_at.strftime("%d %b, %Y")
      json.time created_at.strftime("%H:%M %Z")
    end
  end

  def transaction_total
    
  end

end
