class OrderItem < ActiveRecord::Base
  belongs_to :order
  belongs_to :product_option
  #has_one :product_option
  belongs_to :product
  has_one :product

  def to_builder
    Jbuilder.new do |json|
      json.id id
      json.product_option_id product_option_id
      json.price product_option.price
      json.product product_option.product.name
      json.weight product_option.weight
    end
  end
end
