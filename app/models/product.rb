class Product < ActiveRecord::Base
  belongs_to :order_item
  has_many :orders
  has_many :pictures
  has_many :product_options

  def to_builder
    Jbuilder.new do |json|
    	json.id id
    	json.name name
    	json.description description
    	json.product_options product_options.map { |product_option| product_option.to_builder.attributes!}
    	json.pictures pictures.map { |picture| picture.to_builder.attributes!}
    end
  end
end
