class ProductOption < ActiveRecord::Base
  belongs_to :product
  belongs_to :weight
  belongs_to :order_item
  has_many :order_items

   def to_builder
    Jbuilder.new do |json|
      json.id id
      json.price price
      json.weight weight.name
      json.product product.name
      json.order_items order_items.map { |order_item| order_item.to_builder.attributes!}

    end
  end

end
