class ProductOptionsController < ApplicationController
  def index
    @product_options = ProductOptions.all
  end

  def show
    @product_option = ProductOption.find(params[:id])
  end
end
