class Api::V1::OrderItemsController < Api::V1::ApiController

  skip_before_filter :verify_authenticity_token
  respond_to :json

  def index
    @order_items = OrderItem.all
    product_options_array = @order_items.map {|p| p.to_builder.attributes!}
    render :json => {
      :success => true,
      :status => 200,
      :order_items => order_items_array
    }
  end

  def show
    @order_item = OrderItem.find_by_id(params[:id])
    if @order_item.nil?
      render :json => {
        :success => false,
        :status => 400,
        :message => 'Product Option not found'
    }
    else
      render :json => {
        :success => true,
        :status => 200,
        :order_item => @order_item.to_builder.attributes!
    }
    end
  end

end
