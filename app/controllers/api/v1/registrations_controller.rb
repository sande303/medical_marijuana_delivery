class Api::V1::RegistrationsController < Api::V1::ApiController

  skip_before_filter :verify_authenticity_token
  skip_before_filter :authenticate_user!, only: [:create]
  before_filter :ensure_params_exist, only: [:create, :update]
  respond_to :json

  def create
    @user = User.new(user_params)
    if @user.save
      render :json => {
        :success => true,
        :status => 201,
        :user => {
          :id => @user.id,
          :first_name => @user.first_name,
          :last_name => @user.last_name,
          :city => @user.city,
          :state => @user.state,
          :zip => @user.zip,
          :authentication_token => @user.authentication_token
        }
      }
      return
    else
      render :json =>{
        :success=> false,
        :status => 422,
        :message => @user.errors.full_messages.first
      }
    end
  end

  def show
    @user = User.find_by_authentication_token(params[:authentication_token])
    if @user.nil?
      render :json => {
        :success => false,
        :status => 400,
        :message => 'User not find'
    }
    else
      render :json => {
        :success => true,
        :status => 200,
        :user => {
          :email => current_user.email,
          :first_name => current_user.first_name,
          :last_name => current_user.last_name,
          :address => current_user.address,
          :city => current_user.city,
          :state => current_user.state,
          :zip => current_user.zip,
          :authentication_token => current_user.authentication_token
        }
    }
    end
  end

  def update
    if current_user.update_attributes(user_params)
      render json: {
        status: 201,
        success: true,
        user: {email: current_user.email, first_name: current_user.first_name, last_name: current_user.last_name, address: current_user.address, city: current_user.city, state: current_user.state, zip: current_user.zip, authentication_token: current_user.authentication_token}
      }
    else
      render json: {
        status: 400,
        success: false,
        message: current_user.errors.full_messages
      }
    end
  end

  private

  def ensure_params_exist
    return unless params[:user].nil?
    render json: {
      :status => 400,
      :success => false,
      :message => 'There is missing data'
    }
  end

  def user_params
    params.require(:user).permit(:first_name, :last_name, :address, :city, :state, :zip, :email, :password, :password_confirmation, :authentication_token)
  end

end
