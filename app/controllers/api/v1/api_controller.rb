class Api::V1::ApiController < ApplicationController

	skip_before_filter :verify_authenticity_token!
	before_filter :authenticate_user_from_token!
	respond_to :json

	private

  def authenticate_user_from_token!
    user_token = params[:authentication_token].presence
    user       = user_token && User.find_by_authentication_token(user_token)

    if user
      # Notice we are passing store false, so the user is not
      # actually stored in the session and a token is needed
      # for every request. If you want the token to work as a
      # sign in token, you can simply remove store: false.
      sign_in user, store: false
    else
      render :json => {:success => false, :message => "Error with your credentials", :status => 401}
    end
  end
end