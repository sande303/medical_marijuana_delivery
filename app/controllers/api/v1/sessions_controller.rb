
class Api::V1::SessionsController < Api::V1::ApiController


  skip_before_filter :verify_authenticity_token
 # skip_before_action :verify_authenticity_token
  skip_before_filter :authenticate_user_from_token!, :only => [:create, :status]
  before_filter :ensure_params_exist, :except => [:destroy, :status]
  respond_to :json

  def create
    resource = User.find_for_database_authentication(:email => user_params[:email])
    return invalid_login_attempt unless resource

    if resource.valid_password?(user_params[:password])
      resource.ensure_authentication_token!
      render :json => {
        :success => true,
        :authentication_token => resource.authentication_token,
        :email => resource.email
      }
      return
    end
    invalid_login_attempt
  end

  def destroy
    current_user.reset_authentication_token
    render :json => {
      :success => true,
      :status => 200
    }
  end

  def status
    user_token = params[:authentication_token].presence
    user = user_token && User.find_by_authentication_token(user_token)
    if user
      if user.email == params[:email]
        render :json => {
          :status => 200,
          :success => true,
          :message => 'Token is valid'
        }
      else
        render :json => {
          :status => 400,
          :success => false,
          :message => 'Token is invalid'
        }
      end
    else
      render :json => {
        :status => 400,
        :success => false,
        :message => 'No user found for that token'
      }
    end
  end

  private

  def user_params
    params.require(:user).permit(:email, :password, :encrypted_password, :authentication_token)
  end



  protected

  def ensure_params_exist
    return unless user_params[:email].blank? and user_params[:password].blank?
    render :json => {
      :success => false,
      :message => "missing email or password parameter",
      :status => 422
    }
  end

  def invalid_login_attempt
    render :json => {
      :success => false,
      :message => "Error with your login or password",
      :status => 401
    }
  end


end
