class Api::V1::ProductsController < Api::V1::ApiController

  skip_before_filter :verify_authenticity_token
  respond_to :json

  def index
    @products = Product.all
    products_array = @products.map {|p| p.to_builder.attributes!}
    render :json => {
      :success => true,
      :status => 200,
      :products => products_array
    }
  end

  def show
    @product = Product.find_by_id(params[:id])
    if @product.nil?
      render :json => {
        :success => false,
        :status => 400,
        :message => 'Product not find'
    }
    else
      render :json => {
        :success => true,
        :status => 200,
        :product => @product.to_builder.attributes!
    }
    end
  end

end
