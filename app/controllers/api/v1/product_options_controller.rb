class Api::V1::ProductOptionsController < Api::V1::ApiController

  skip_before_filter :verify_authenticity_token
  respond_to :json

  def index
    @product_options = ProductOption.all
    product_options_array = @product_options.map {|p| p.to_builder.attributes!}
    render :json => {
      :success => true,
      :status => 200,
      :product_options => product_options_array
    }
  end

  def show
    @product_option = ProductOption.find_by_id(params[:id])
    if @product_option.nil?
      render :json => {
        :success => false,
        :status => 400,
        :message => 'Product Option not found'
    }
    else
      render :json => {
        :success => true,
        :status => 200,
        :product_option => @product_option.to_builder.attributes!
    }
    end
  end

end
