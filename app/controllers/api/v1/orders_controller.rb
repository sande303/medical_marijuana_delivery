module Api
  class V1::OrdersController < Api::V1::ApiController

    skip_before_filter :verify_authenticity_token

    def create
      @order = Order.create!(user_id: current_user.id)

      # product_option_ids = params[:order][:product_option_ids]
      # product_option_ids.each do |i|
      #   order_item = OrderItem.create!(product_option_id: i, order_id: @order.id)
      # end

      product_option_ids = params[:order]
      product_option_ids.each do |product_option_id, quantity|
        quantity.times do |i|
          order_item = OrderItem.create!(product_option_id: product_option_id, order_id: @order.id)
        end
      end

      render :json => {
          :success => true,
          :status => 200,
          :order_id => @order.id
        }


    end

    def index
      @orders = Order.order('id DESC')
      orders_array = @orders.map {|o| o.to_builder.attributes!}
      render :json => {
        :success => true,
        :status => 200,
        :orders => orders_array
      }
    end

    def show
      @order = @current_order
      if @order.nil?
        render :json => {
          :success => false,
          :status => 400,
          :message => 'Order does not exist'
      }
      else
        render :json => {
          :success => true,
          :status => 200,
          :order => @order.to_builder.attributes!
      }

      end

    end
  end
end
