module Api
  class V1::UsersController < Api::V1::ApiController
    def index
      @users = User.all
      render :json => {
        :success => true,
        :status => 200,
        :users => @users
      }
    end

    def show
      @user = User.find_by_authentication_token(params[:authentication_token])
      if @user.nil?
        render :json => {
          :success => false,
          :status => 400,
          :message => 'User not find'
      }
      else
        render :json => {
          :success => true,
          :status => 200,
          :user => @user
      }
      end
    end
  end
end