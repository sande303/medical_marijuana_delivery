class Api::V1::PreferencesController < Api::V1::ApiController

  skip_before_filter :verify_authenticity_token
  respond_to :json

  def index
    @preferences = Preference.all
    preference_array = @preferences.map {|p| p.to_builder.attributes!}
    render :json => {
      :success => true,
      :status => 200,
      :preferences => preference_array
    }
  end

  def show
    @preference = Preference.find_by_id(params[:id])
    if @preference.nil?
      render :json => {
        :success => false,
        :status => 400,
        :message => 'Preference not found'
      }
    else
      render :json => {
        :success => true,
        :status => 200,
        :product => @preference.to_builder.atributes!
      }
    end
  end
end
