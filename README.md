# Delivery app API

* All requests must include an authentication_token except signup
* If the user is not found by the authentication_token provided, the response will be:

        {"success":false,"message":"Error with your credentials","status":401}

## Users
### Post /api/v1/signup
* Create a new user
* Request

      @signup.json =
          {"user": {
            "email": "sande303@gmail.com",
            "first_name": "John",
            "last_name": "Sanders",
            "address": "address",
            "city": "city",
            "state": "state",
            "zip": "12345",
            "password": "password"
            }
          }

        curl -X POST -d "@signup.json" 'http://localhost:3000/api/signup'-H 'content-type: application/json'

* Response

        {
          "success": true,
          "status": 201,
          "user": {
            "id": 18,
            "first_name": "John",
            "last_name": "Sanders",
            "authentication_token": "HGx_T_63GTTjNV6GmPbn"
          }
        }

        {"success":false,"status":422,"message": "Email has already benn taken"}

### POST /api/v1/login
* Sign a user into the app
* Request

        @login.json = {
          "email": "sande303@gmail.com",
          "password": "password"
        }

        curl -X POST -d "@login" 'http://localhost:3000/api/v1/login' -H 'content-type: application/json'

* Response

        {
          "success": true,
          "authentication_token": "HGx_T_63GTTjNV6GmPbn",
          "email": "sande303@gmail.com"
        }

        {"success":false,"message": "Error with your login or password","status":401}

### Delete /api/v1/signout
* Sign out a user
* Must include the authentication_token
* Destroys the user's authentication_token and creates a new one
* Request

        curl -X DELETE 'http://localhost:3000/api/v1/signout?authentication_token=HGx_T_63GTTjNV6GmPbn' -H 'Content-type: application/json'

* Response

        {"success":true,"status":200}

        {"success":false,"message":"Error with your credentials","status":401}

### GET /api/v1/users/status
* Checks if a user can be found by the authentication token and verifies that the user found by authentication_token matches the email set
* Expects an authentication_token and email
* Request

        curl 'http://localhost3000/api/v1/users/status?authentication_token=HGx_T_63GTTjNV6GmPbn&email=sande303@gmail.com' -H 'Content-type: application/json'

* Response

        {"status":200,"success":true,"message":"Token is valid"}

        {"status":400,"success":false,"message":"Token was found but it does not match the user's email"}
        {"status":400,"success":false,"message":""}

### POST /api/v1/user
* updates the users information
* Expects an authentication_token 
* Request

        curl -X PUT 'http://localhost3000/api/v1/user?authentication_token=HGx_T_63GTTjNV6GmPbn
        
        {"user": {
            "email": "sande303@gmail.com",
            "first_name": "My",
            "last_name": "New",
            "address": "Informaiton",
            "city": "city",
            "state": "state",
            "zip": "12345",
            "password": "password"
            }
          }   

* Response

        {"user": {
            "email": "sande303@gmail.com",
            "first_name": "My",
            "last_name": "New",
            "address": "Information",
            "city": "city",
            "state": "state",
            "zip": "12345",
            "password": "password"
            }
          }
